// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator

@ccclass
export default class GameOverLayout extends cc.Component {
    gameScore: number = 0
    curLevel: number
    gameLayout = null

    @property(cc.Label)
    lblScore: cc.Label = null

    @property(cc.Label)
    lblLabel: cc.Label = null

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout')
    }

    checkLevel() {
        switch (this.curLevel) {
            case 1:
                this.gameLayout.getComponent('GameLayout').initLevel(1)
                break
            case 2:
                this.gameLayout.getComponent('GameLayout').initLevel(2)
                break
            case 3:
                this.gameLayout.getComponent('GameLayout').initLevel(3)
                break
            case 4:
                this.gameLayout.getComponent('GameLayout').initLevel(4)
                break
            case 5:
                this.gameLayout.getComponent('GameLayout').initLevel(5)
                break
            case 6:
                this.gameLayout.getComponent('GameLayout').initLevel(6)
                break
            case 7:
                this.gameLayout.getComponent('GameLayout').initLevel(7)
                break
            case 8:
                this.gameLayout.getComponent('GameLayout').initLevel(8)
                break
            case 9:
                this.gameLayout.getComponent('GameLayout').initLevel(9)
                break
            case 10:
                this.gameLayout.getComponent('GameLayout').initLevel(10)
                break
            case 11:
                this.gameLayout.getComponent('GameLayout').initLevel(11)
                break
            case 12:
                this.gameLayout.getComponent('GameLayout').initLevel(12)
                break
            case 13:
                this.gameLayout.getComponent('GameLayout').initLevel(13)
                break
            case 14:
                this.gameLayout.getComponent('GameLayout').initLevel(14)
                break
            case 15:
                this.gameLayout.getComponent('GameLayout').initLevel(15)
                break
            case 16:
                this.gameLayout.getComponent('GameLayout').initLevel(16)
                break
            case 17:
                this.gameLayout.getComponent('GameLayout').initLevel(17)
                break
            case 18:
                this.gameLayout.getComponent('GameLayout').initLevel(18)
                break
        }
        this.node.active = false
        this.gameLayout.active = true
    }

    update(dt) {
        if (this.gameScore >= 999999) {
            this.lblScore.string = '999999'
        } else {
            this.lblScore.string = this.gameScore.toString()
        }
        this.lblLabel.string = this.curLevel.toString()
    }
}
