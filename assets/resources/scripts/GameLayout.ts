// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import GameScene from "./GameScene"
import { SoundType } from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

export enum GameStatus {
    Game_Ready = 0,
    Game_Playing,
    Game_Over
}

@ccclass
export default class GameLayout extends cc.Component {
    gameStatus: GameStatus = GameStatus.Game_Ready
    gameOverLayout = null
    gameControl: GameScene = null
    gameScore: number = 0
    bonusScore: number = 500
    time: number
    totalCard: number = 6
    unlocked: number
    curLevel: number

    posList = []
    currentList = []
    cardList = []
    correctNumb: number = 0

    @property(cc.Node)
    card1: cc.Node = null
    @property(cc.Node)
    card2: cc.Node = null
    @property(cc.Node)
    card3: cc.Node = null
    @property(cc.Node)
    card4: cc.Node = null
    @property(cc.Node)
    card5: cc.Node = null
    @property(cc.Node)
    card6: cc.Node = null
    @property(cc.Node)
    card7: cc.Node = null
    @property(cc.Node)
    card8: cc.Node = null
    @property(cc.Node)
    card9: cc.Node = null
    @property(cc.Node)
    card10: cc.Node = null
    @property(cc.Node)
    card11: cc.Node = null
    @property(cc.Node)
    card12: cc.Node = null
    @property(cc.Node)
    card13: cc.Node = null
    @property(cc.Node)
    card14: cc.Node = null
    @property(cc.Node)
    card15: cc.Node = null
    @property(cc.Node)
    card16: cc.Node = null
    @property(cc.Node)
    card17: cc.Node = null
    @property(cc.Node)
    card18: cc.Node = null
    @property(cc.Node)
    card19: cc.Node = null
    @property(cc.Node)
    card20: cc.Node = null

    @property(cc.Label)
    lblScore: cc.Label = null
    @property(cc.Label)
    lblTime: cc.Label = null

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.gameScore = Number(cc.sys.localStorage.getItem('score'))
        this.lblScore.string = this.gameScore.toString()
        // this.lblTime.string = this.time.toString()
        this.gameOverLayout = cc.Canvas.instance.node.getChildByName('GameOverLayout')
        this.gameControl = cc.Canvas.instance.node.getComponent('GameScene')
        this.unlocked = Number(cc.sys.localStorage.getItem('unlocked'))
    }

    initLevel(level: number) {
        switch (level) {
            case 1:
                this.time = 61
                this.bonusScore = 500
                this.totalCard = 6
                this.posList = [
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(-70, -162),
                    cc.v2(70, -162)
                ]
                this.cardList = [this.card1, this.card2, this.card3, this.card11, this.card12, this.card13]
                this.curLevel = level
                break
            case 2:
                this.time = 51
                this.bonusScore = 1000
                this.totalCard = 6
                this.posList = [
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(-70, -162),
                    cc.v2(70, -162)
                ]
                this.cardList = [this.card1, this.card2, this.card3, this.card11, this.card12, this.card13]
                this.curLevel = level
                break
            case 3:
                this.time = 41
                this.bonusScore = 1500
                this.totalCard = 6
                this.posList = [
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(-70, -162),
                    cc.v2(70, -162)
                ]
                this.cardList = [this.card1, this.card2, this.card3, this.card11, this.card12, this.card13]
                this.curLevel = level
                break
            case 4:
                this.time = 81
                this.bonusScore = 2000
                this.totalCard = 12
                this.posList = [
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162)
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3, this.card11, this.card12, this.card13,
                    this.card4, this.card5, this.card6, this.card14, this.card15, this.card16]
                this.curLevel = level
                break
            case 5:
                this.time = 71
                this.bonusScore = 2500
                this.totalCard = 12
                this.posList = [
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162)
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3, this.card11, this.card12, this.card13,
                    this.card4, this.card5, this.card6, this.card14, this.card15, this.card16]
                this.curLevel = level
                break
            case 6:
                this.time = 61
                this.bonusScore = 3000
                this.totalCard = 12
                this.posList = [
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162)
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3, this.card11, this.card12, this.card13,
                    this.card4, this.card5, this.card6, this.card14, this.card15, this.card16]
                this.curLevel = level
                break
            case 7:
                this.time = 56
                this.bonusScore = 3500
                this.totalCard = 12
                this.posList = [
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162)
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3, this.card11, this.card12, this.card13,
                    this.card4, this.card5, this.card6, this.card14, this.card15, this.card16]
                this.curLevel = level
                break
            case 8:
                this.time = 51
                this.bonusScore = 4000
                this.totalCard = 12
                this.posList = [
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162)
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3, this.card11, this.card12, this.card13,
                    this.card4, this.card5, this.card6, this.card14, this.card15, this.card16]
                this.curLevel = level
                break
            case 9:
                this.time = 51
                this.bonusScore = 4500
                this.totalCard = 12
                this.posList = [
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162)
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3, this.card11, this.card12, this.card13,
                    this.card4, this.card5, this.card6, this.card14, this.card15, this.card16]
                this.curLevel = level
                break
            case 10:
                this.time = 51
                this.bonusScore = 5000
                this.totalCard = 12
                this.posList = [
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162)
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3, this.card11, this.card12, this.card13,
                    this.card4, this.card5, this.card6, this.card14, this.card15, this.card16]
                this.curLevel = level
                break
            case 11:
                this.time = 51
                this.bonusScore = 5500
                this.totalCard = 12
                this.posList = [
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162)
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3, this.card11, this.card12, this.card13,
                    this.card4, this.card5, this.card6, this.card14, this.card15, this.card16]
                this.curLevel = level
                break
            case 12:
                this.time = 101
                this.bonusScore = 6000
                this.totalCard = 20
                this.posList = [
                    cc.v2(-350, 191),
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(350, 191),
                    cc.v2(-490, 15),
                    cc.v2(-350, 15),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(350, 15),
                    cc.v2(490, 15),
                    cc.v2(-350, -162),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162),
                    cc.v2(350, -162),
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3,
                    this.card4, this.card5, this.card6,
                    this.card7, this.card8, this.card9, this.card10,
                    this.card11, this.card12, this.card13,
                    this.card14, this.card15, this.card16,
                    this.card17, this.card18, this.card19, this.card20
                ]
                this.curLevel = level
                break
            case 13:
                this.time = 91
                this.bonusScore = 6500
                this.totalCard = 20
                this.posList = [
                    cc.v2(-350, 191),
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(350, 191),
                    cc.v2(-490, 15),
                    cc.v2(-350, 15),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(350, 15),
                    cc.v2(490, 15),
                    cc.v2(-350, -162),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162),
                    cc.v2(350, -162),
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3,
                    this.card4, this.card5, this.card6,
                    this.card7, this.card8, this.card9, this.card10,
                    this.card11, this.card12, this.card13,
                    this.card14, this.card15, this.card16,
                    this.card17, this.card18, this.card19, this.card20
                ]
                this.curLevel = level
                break
            case 14:
                this.time = 81
                this.bonusScore = 7000
                this.totalCard = 20
                this.posList = [
                    cc.v2(-350, 191),
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(350, 191),
                    cc.v2(-490, 15),
                    cc.v2(-350, 15),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(350, 15),
                    cc.v2(490, 15),
                    cc.v2(-350, -162),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162),
                    cc.v2(350, -162),
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3,
                    this.card4, this.card5, this.card6,
                    this.card7, this.card8, this.card9, this.card10,
                    this.card11, this.card12, this.card13,
                    this.card14, this.card15, this.card16,
                    this.card17, this.card18, this.card19, this.card20
                ]
                this.curLevel = level
                break
            case 15:
                this.time = 71
                this.bonusScore = 7500
                this.totalCard = 20
                this.posList = [
                    cc.v2(-350, 191),
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(350, 191),
                    cc.v2(-490, 15),
                    cc.v2(-350, 15),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(350, 15),
                    cc.v2(490, 15),
                    cc.v2(-350, -162),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162),
                    cc.v2(350, -162),
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3,
                    this.card4, this.card5, this.card6,
                    this.card7, this.card8, this.card9, this.card10,
                    this.card11, this.card12, this.card13,
                    this.card14, this.card15, this.card16,
                    this.card17, this.card18, this.card19, this.card20
                ]
                this.curLevel = level
                break
            case 16:
                this.time = 66
                this.bonusScore = 8000
                this.totalCard = 20
                this.posList = [
                    cc.v2(-350, 191),
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(350, 191),
                    cc.v2(-490, 15),
                    cc.v2(-350, 15),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(350, 15),
                    cc.v2(490, 15),
                    cc.v2(-350, -162),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162),
                    cc.v2(350, -162),
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3,
                    this.card4, this.card5, this.card6,
                    this.card7, this.card8, this.card9, this.card10,
                    this.card11, this.card12, this.card13,
                    this.card14, this.card15, this.card16,
                    this.card17, this.card18, this.card19, this.card20
                ]
                this.curLevel = level
                break
            case 17:
                this.time = 61
                this.bonusScore = 8500
                this.totalCard = 20
                this.posList = [
                    cc.v2(-350, 191),
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(350, 191),
                    cc.v2(-490, 15),
                    cc.v2(-350, 15),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(350, 15),
                    cc.v2(490, 15),
                    cc.v2(-350, -162),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162),
                    cc.v2(350, -162),
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3,
                    this.card4, this.card5, this.card6,
                    this.card7, this.card8, this.card9, this.card10,
                    this.card11, this.card12, this.card13,
                    this.card14, this.card15, this.card16,
                    this.card17, this.card18, this.card19, this.card20
                ]
                this.curLevel = level
                break
            case 18:
                this.time = 56
                this.bonusScore = 9000
                this.totalCard = 20
                this.posList = [
                    cc.v2(-350, 191),
                    cc.v2(-210, 191),
                    cc.v2(-70, 191),
                    cc.v2(70, 191),
                    cc.v2(210, 191),
                    cc.v2(350, 191),
                    cc.v2(-490, 15),
                    cc.v2(-350, 15),
                    cc.v2(-210, 15),
                    cc.v2(-70, 15),
                    cc.v2(70, 15),
                    cc.v2(210, 15),
                    cc.v2(350, 15),
                    cc.v2(490, 15),
                    cc.v2(-350, -162),
                    cc.v2(-210, -162),
                    cc.v2(-70, -162),
                    cc.v2(70, -162),
                    cc.v2(210, -162),
                    cc.v2(350, -162),
                ]
                this.cardList = [
                    this.card1, this.card2, this.card3,
                    this.card4, this.card5, this.card6,
                    this.card7, this.card8, this.card9, this.card10,
                    this.card11, this.card12, this.card13,
                    this.card14, this.card15, this.card16,
                    this.card17, this.card18, this.card19, this.card20
                ]
                this.curLevel = level
                break
        }
        this.onPlay()
    }

    onPlay(isReplay: boolean = true) {
        this.gameStatus = GameStatus.Game_Playing
        this.gameScore = Number(cc.sys.localStorage.getItem('score'))
        // this.gameScore = 0
        this.lblScore.string = this.gameScore.toString()
        this.currentList = []
        this.correctNumb = 0
        this.onSpawn()
        this.countdown(this.time)
    }

    checkCard(event) {
        if (this.currentList.length < 2) {
            // open card
            event.target.children[0].active = true
            event.target.getComponent(cc.Button).enabled = false
            this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Flip)

            this.currentList.push(event.target)
            setTimeout(() => {
                if (this.currentList.length >= 2) {
                    // check is matched ? 
                    if (this.currentList[0].children[0].name.indexOf(this.currentList[1].children[0].name) !== -1) {
                        for (let item of this.currentList) {
                            item.children[0].active = false
                            item.getComponent(cc.Button).enabled = true
                            item.active = false
                        }
                        this.correctNumb += 2
                        this.gameScore = this.gameScore + 100
                        this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Scored)
                    } else {
                        // close cards
                        for (let item of this.currentList) {
                            item.children[0].active = false
                            item.getComponent(cc.Button).enabled = true
                        }
                    }
                    this.currentList = []
                }
            }, 1000)
        } else {
            return
        }
    }

    onSpawn() {
        let maxIndex = this.totalCard - 1
        for (let card of this.cardList) {
            card.active = true
            let index = Math.floor(0 + Math.random() * (maxIndex + 1 - 0))
            let pos = this.posList.splice(index, 1)[0]
            card.position = pos
            maxIndex = maxIndex - 1
        }
    }

    gameOver(isUnlocked: boolean) {
        if (isUnlocked) {
            if (this.curLevel == this.unlocked) {
                cc.sys.localStorage.setItem('unlocked', this.unlocked + 1)
                this.unlocked = Number(cc.sys.localStorage.getItem('unlocked'))            // cc.sys.localStorage.setItem('unlocked', 3)
            }
        }
        this.gameStatus = GameStatus.Game_Over
        this.bonusScore = 0
        this.node.stopAllActions()
        this.unscheduleAllCallbacks()
        cc.Canvas.instance.node.getChildByName('GameLayout').active = false
        this.gameOverLayout.active = true
        this.gameOverLayout.getComponent('GameOverLayout').gameScore = this.gameScore
        this.gameOverLayout.getComponent('GameOverLayout').curLevel = this.curLevel
    }

    countdown(time: number) {
        --time
        this.lblTime.string = time.toString()
        if (time > 0) {
            this.scheduleOnce(() => this.countdown(time), 1)
        } else {
            this.gameOver(false)
        }
        this.lblTime.string = time.toString()
    }


    start() {
    }

    update(dt) {
        if (this.correctNumb >= this.totalCard) {
            this.gameScore += this.bonusScore
            cc.sys.localStorage.setItem('score', this.gameScore)
            this.gameOver(true)
        }
        if (this.gameStatus === GameStatus.Game_Playing) {
            cc.sys.localStorage.setItem('score', this.gameScore)
            if (this.gameScore >= 999999) {
                this.lblScore.string = '999999'
            } else {
                this.lblScore.string = this.gameScore.toString()
            }
            // this.bonusScore -= 1
            // if (this.bonusScore <= 0) {
            //     this.bonusScore = 0
            // }
        }
    }
}
