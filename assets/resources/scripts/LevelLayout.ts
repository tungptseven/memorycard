// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator

@ccclass
export default class LevelLayout extends cc.Component {
    unlocked: number
    gameLayout = null
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout')
        this.unlocked = Number(cc.sys.localStorage.getItem('unlocked'))
        // this.unlockLevel(this.unlocked)
    }

    unlockLevel(level: number) {
        for (let i = 0;i < level;i++) {
            this.node.getChildByName('scrollView').getChildByName('levels').children[i].children[1].active = true
        }
    }

    checkLevel(event) {
        let level = event.target.parent.name
        switch (level) {
            case 'lv1':
                this.gameLayout.getComponent('GameLayout').initLevel(1)
                break
            case 'lv2':
                this.gameLayout.getComponent('GameLayout').initLevel(2)
                break
            case 'lv3':
                this.gameLayout.getComponent('GameLayout').initLevel(3)
                break
            case 'lv4':
                this.gameLayout.getComponent('GameLayout').initLevel(4)
                break
            case 'lv5':
                this.gameLayout.getComponent('GameLayout').initLevel(5)
                break
            case 'lv6':
                this.gameLayout.getComponent('GameLayout').initLevel(6)
                break
            case 'lv7':
                this.gameLayout.getComponent('GameLayout').initLevel(7)
                break
            case 'lv8':
                this.gameLayout.getComponent('GameLayout').initLevel(8)
                break
            case 'lv9':
                this.gameLayout.getComponent('GameLayout').initLevel(9)
                break
            case 'lv10':
                this.gameLayout.getComponent('GameLayout').initLevel(10)
                break
            case 'lv11':
                this.gameLayout.getComponent('GameLayout').initLevel(11)
                break
            case 'lv12':
                this.gameLayout.getComponent('GameLayout').initLevel(12)
                break
            case 'lv13':
                this.gameLayout.getComponent('GameLayout').initLevel(13)
                break
            case 'lv14':
                this.gameLayout.getComponent('GameLayout').initLevel(14)
                break
            case 'lv15':
                this.gameLayout.getComponent('GameLayout').initLevel(15)
                break
            case 'lv16':
                this.gameLayout.getComponent('GameLayout').initLevel(16)
                break
            case 'lv17':
                this.gameLayout.getComponent('GameLayout').initLevel(17)
                break
            case 'lv18':
                this.gameLayout.getComponent('GameLayout').initLevel(18)
                break
        }
        this.node.active = false
        this.gameLayout.active = true
    }

    start() {
        // this.unlockLevel(this.unlocked)
    }

    update(dt) {
        if (this.unlocked >= 18) {
            cc.sys.localStorage.setItem('unlocked', 18)
        }
        this.unlocked = Number(cc.sys.localStorage.getItem('unlocked'))
        this.unlockLevel(this.unlocked)
    }
}
