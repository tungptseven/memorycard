import GameLayout from "./GameLayout"
import HomeLayout from "./HomeLayout"
import AudioSourceControl from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class GameScene extends cc.Component {
    unlocked: number
    isMuted: boolean = false

    @property(GameLayout)
    gameLayout: GameLayout = null

    @property(HomeLayout)
    homeLayout: HomeLayout = null

    @property(cc.Node)
    nHowTo: cc.Node = null

    @property(cc.Node)
    nGameOver: cc.Node = null

    @property(cc.Node)
    nLevel: cc.Node = null

    @property(AudioSourceControl)
    audioSourceControl: AudioSourceControl = null

    // LIFE-CYCLE CALLBACKS:

    onPlay() {
        this.gameLayout.node.active = true
        this.homeLayout.node.active = false
        this.nHowTo.active = false
        this.nLevel.active = false
        this.nGameOver.active = false
        // this.gameLayout.onPlay(false)
        this.gameLayout.initLevel(this.unlocked)
        this.audioSourceControl.onStart(this.isMuted)
    }

    onHome() {
        this.gameLayout.node.active = false
        this.homeLayout.node.active = true
        this.nHowTo.active = false
        this.nLevel.active = false
        this.nGameOver.active = false
        this.audioSourceControl.onStart(this.isMuted)
    }

    onReplay() {
        this.gameLayout.node.active = true
        this.homeLayout.node.active = false
        this.nHowTo.active = false
        this.nLevel.active = false
        this.nGameOver.active = false
        this.gameLayout.onPlay()
        this.audioSourceControl.onStart(this.isMuted)
    }

    onHowToOpen() {
        this.nHowTo.active = true
    }

    onHowToClose() {
        this.gameLayout.node.active = false
        this.homeLayout.node.active = true
        this.nHowTo.active = false
        this.nLevel.active = false
        this.nGameOver.active = false
    }

    onLevelOpen() {
        this.gameLayout.node.active = false
        this.homeLayout.node.active = false
        this.nHowTo.active = false
        this.nLevel.active = true
        this.nGameOver.active = false
    }

    onLevelClose() {
        this.gameLayout.node.active = false
        this.homeLayout.node.active = true
        this.nHowTo.active = false
        this.nLevel.active = false
        this.nGameOver.active = false
    }

    onSoundToggle() {
        this.isMuted = !this.isMuted
        if (this.isMuted) {
            this.audioSourceControl.pause()
            cc.Canvas.instance.node.getChildByName('HomeLayout').getChildByName('btnSoundOff').active = false
            cc.Canvas.instance.node.getChildByName('HomeLayout').getChildByName('btnSoundOn').active = true
        } else {
            this.audioSourceControl.resume()
            cc.Canvas.instance.node.getChildByName('HomeLayout').getChildByName('btnSoundOff').active = true
            cc.Canvas.instance.node.getChildByName('HomeLayout').getChildByName('btnSoundOn').active = false
        }
    }

    onGameOver() { }

    onLoad() {
        this.gameLayout.node.active = false
        this.homeLayout.node.active = true
        this.audioSourceControl.onStart(this.isMuted)
        cc.sys.localStorage.setItem('unlocked', 1)
        cc.sys.localStorage.setItem('score', 0)
        this.unlocked = Number(cc.sys.localStorage.getItem('unlocked'))
    }

    start() {

    }

    // update (dt) {}
}