
const { ccclass, property } = cc._decorator

export enum SoundType {
    E_Sound_Flip = 0,
    E_Sound_Scored,
    E_Sound_Background
}
@ccclass
export default class AudioSourceControl extends cc.Component {

    @property({ type: cc.AudioClip })
    backgroundMusic: cc.AudioClip = null

    // sound effect when bird flying
    @property({ type: cc.AudioClip })
    flipSound: cc.AudioClip = null

    @property({ type: cc.AudioClip })
    scoredSound: cc.AudioClip = null


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onStart(isMute: boolean) {
        if (!isMute) {
            cc.audioEngine.playMusic(this.backgroundMusic, true)
        } else { return }
    }

    playSound(type: SoundType) {
        if (type == SoundType.E_Sound_Flip) {
            cc.audioEngine.playEffect(this.flipSound, false)
        }
        else if (type == SoundType.E_Sound_Scored) {
            cc.audioEngine.playEffect(this.scoredSound, false)
        }
    }

    pause() {
        cc.audioEngine.pauseMusic()
    }

    resume() {
        cc.audioEngine.resumeMusic()
    }
    // update (dt) {}
}
